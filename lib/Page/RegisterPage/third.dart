import 'package:auth/Components/widgets.dart';
import 'package:auth/Styling/styles.dart';
import 'package:auth/generated/l10n.dart';
import 'package:flutter/material.dart';
import '../../main.dart';

class ThirdPage extends StatefulWidget {
  @override
  _ThirdPageState createState() => _ThirdPageState();
}

class _ThirdPageState extends State<ThirdPage> {
  final FocusNode _nameControllerFocus = FocusNode();
  final _emailController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Positioned.fill(
            child: BackgroundWidget(
              imagepath:
                  'https://image.freepik.com/free-vector/worldwide-connection-gray-background-illustration-vector_53876-61769.jpg',
            ),
          ),
          Center(
              child: Container(
                  alignment: Alignment.center,
                  child: SingleChildScrollView(
                      //constraints: BoxConstraints.expand(),
                      child: Center(
                          child: Form(
                    child: Container(
                        child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Flutterlogo(
                          size: 60.0,
                        ),
                        SizedBox(
                          height: 1,
                        ),
                        Container(
                          child: Column(children: <Widget>[
                            SizedBox(
                              height: 30,
                            ),
                            TextFormFieldWidget(
                              hintText: S.of(context).name,
                              prefixIcon: Icon(Icons.email),
                              textInputType: TextInputType.emailAddress,
                              actionKeyboard: TextInputAction.done,
                              functionValidate: commonValidation,
                              controller: _emailController,
                              focusNode: _nameControllerFocus,
                            ),
                            SizedBox(
                              height: 20,
                            ),
                            raisedButton(
                              textColor: button_textcolor,
                              text: S.of(context).create,
                              height: 50.0,
                              borderRadius: 5,
                              color: button_primaryColor,
                              borderSideColor: button_borderSideColor,
                              splashColor: Colors.blue[200],
                              style: TextStyle(
                                color: button_primaryColor,
                                fontSize: 14.0,
                                fontWeight: FontWeight.w500,
                                fontStyle: fontdesign,
                                letterSpacing: 1.2,
                              ),
                              onClick: () =>
                                  Routes.sailor.navigate("loginpage"),
                            ),
                          ]),
                        ),
                      ],
                    )),
                  ))))),
        ],
      ),
    );
  }
}