import 'package:auth/Components/widgets.dart';
import 'package:auth/Styling/SizeConfig.dart';
import 'package:auth/Styling/styles.dart';
import 'package:auth/generated/l10n.dart';
import 'package:flutter/material.dart';
import '../../main.dart';

class SecondPage extends StatefulWidget {
  @override
  _SecondPageState createState() => _SecondPageState();
}

class _SecondPageState extends State<SecondPage> {
  final _passwordController = TextEditingController();
  final FocusNode _nameControllerFocus = FocusNode();
  final FocusNode _passwordControllerFocus = FocusNode();
  final _emailController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Positioned.fill(
            child: BackgroundWidget(
              imagepath:
                  'https://image.freepik.com/free-vector/worldwide-connection-gray-background-illustration-vector_53876-61769.jpg',
            ),
          ),
          Center(
              child: Container(
                  constraints: BoxConstraints.expand(),
                  child: Center(
                      child: Form(
                    child: Container(
                        alignment: Alignment.center,
                        child: SingleChildScrollView(
                            child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Flutterlogo(
                              size: 60.0,
                            ),
                            SizedBox(
                              height: 1,
                            ),
                            Container(
                              //height: SizeConfig.height*1, //10 for example
                              width: SizeConfig.width,
                              //child: Card(
                              child: Column(children: <Widget>[
                                SizedBox(
                                  height: 30,
                                ),
                                Container(
                                  child: ListView.separated(
                                      separatorBuilder: (context, index) {
                                        return SizedBox(
                                          height: 10,
                                        );
                                      },
                                      shrinkWrap: true,
                                      itemCount: 2,
                                      itemBuilder:
                                          (BuildContext context, int index) {
                                        return Container(
                                          child: Center(
                                            child: TextFormFieldWidget(
                                              hintText: index == 0
                                                  ? S.of(context).fname
                                                  : S.of(context).password,
                                              prefixIcon: index == 0
                                                  ? Icon(Icons.email)
                                                  : Icon(Icons.lock),
                                              textInputType: index == 0
                                                  ? TextInputType.emailAddress
                                                  : TextInputType
                                                      .visiblePassword,
                                              actionKeyboard: index == 0
                                                  ? TextInputAction.next
                                                  : null,
                                              functionValidate:
                                                  commonValidation,
                                              controller: index == 0
                                                  ? _emailController
                                                  : _passwordController,
                                              focusNode: index == 0
                                                  ? _nameControllerFocus
                                                  : _passwordControllerFocus,
                                              obscureText:
                                                  index == 0 ? false : true,
                                            ),
                                          ),
                                        );
                                      }),
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                raisedButton(
                                    textColor: Colors.white,
                                    text: S.of(context).next,
                                    height: 50.0,
                                    borderRadius: 5,
                                    color: button_primaryColor,
                                    borderSideColor: Colors.white,
                                    splashColor: Colors.blue[200],
                                    style: TextStyle(
                                      color: button_primaryColor,
                                      fontSize: 14.0,
                                      fontWeight: FontWeight.w500,
                                      fontStyle: fontdesign,
                                      letterSpacing: 1.2,
                                    ),
                                    onClick: () =>  Routes.sailor.navigate("third")
                                    ),
                              ]),
                            ),
                          ],
                        ))),
                  ))))
        ],
      ),
    );
  }
}
