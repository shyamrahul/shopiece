import 'package:auth/generated/l10n.dart';
import 'package:flutter/material.dart';
import 'package:auth/Styling/styles.dart';
import 'package:auth/Components/widgets.dart';
import 'package:auth/main.dart';

class RegisterPage extends StatefulWidget {
  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _emailController = TextEditingController();
  final FocusNode _nameControllerFocus = FocusNode();
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Positioned.fill(
            child: BackgroundWidget(
              imagepath:
                  'https://image.freepik.com/free-vector/worldwide-connection-gray-background-illustration-vector_53876-61769.jpg',
            ),
          ),
          Center(
            child: Container(
              alignment: Alignment.center,
              child: SingleChildScrollView(
                //constraints: BoxConstraints.expand(),
                child: Center(
                  child: Form(
                    key: _formKey,
                    child: Container(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Flutterlogo(
                            size: 60.0,
                          ),
                          SizedBox(
                            height: 1,
                          ),
                          Container(
                            child: Form(
                              child: Column(
                                children: <Widget>[
                                  SizedBox(
                                    height: 30,
                                  ),
                                  TextFormFieldWidget(
                                    hintText: S.of(context).name,
                                    prefixIcon: Icon(Icons.email),
                                    textInputType: TextInputType.emailAddress,
                                    actionKeyboard: TextInputAction.done,
                                    functionValidate: commonValidation,
                                    controller: _emailController,
                                    focusNode: _nameControllerFocus,
                                  ),
                                  SizedBox(
                                    height: 20,
                                  ),
                                  raisedButton(
                                    textColor: button_textcolor,
                                    //minWidth: 300,
                                    text: S.of(context).next,
                                    height: 50.0,
                                    borderRadius: 5,
                                    color: button_primaryColor,
                                    borderSideColor: button_borderSideColor,
                                    splashColor: Colors.blue[200],
                                    style: TextStyle(
                                      color: button_primaryColor,
                                      fontSize: 14.0,
                                      fontWeight: FontWeight.w500,
                                      fontStyle: fontdesign,
                                      letterSpacing: 1.2,
                                    ),
                                    onClick: () =>
                                        Routes.sailor.navigate("second"),
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}