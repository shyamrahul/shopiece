import 'package:flutter/material.dart';
import 'package:auth/Styling/styles.dart';
import 'package:auth/Styling/SizeConfig.dart';

class TextFormFieldWidget extends StatefulWidget {
  final Key fieldKey;
  final int maxLength;
  final Widget prefixIcon;
  final String hintText;
  final String labelText;
  final String helperText;
  final bool obscureText;
  final TextInputAction actionKeyboard;
  final TextInputType textInputType;
  final FormFieldSetter<String> onSaved;
  final FormFieldValidator<String> validator;
  final ValueChanged<String> onFieldSubmitted;
  final String defaultText;
  final FocusNode focusNode;
  final TextEditingController controller;
  final Function functionValidate;
  final String parametersValidate;
  final Function onSubmitField;
  final Function onFieldTap;
  var align;

   TextFormFieldWidget({
    this.fieldKey,
    this.maxLength,
    this.hintText,
    this.labelText,
    this.helperText,
    this.onSaved,
    this.obscureText = false,
    this.validator,
    this.actionKeyboard = TextInputAction.next,
    this.onFieldSubmitted,
    this.textInputType,
    this.prefixIcon,
    this.focusNode,
    this.defaultText,
    this.controller,
    this.functionValidate,
    this.parametersValidate,
    this.onSubmitField,
    this.onFieldTap,
    this.align
  });

  @override
  _TextFormFieldWidgetState createState() => _TextFormFieldWidgetState();
}

class _TextFormFieldWidgetState extends State<TextFormFieldWidget> {
  double bottomPaddingToError = 12;

  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Container(
        alignment: widget.align,
        width: SizeConfig.width,
        child: TextFormField(
          key: widget.fieldKey,
          cursorColor: textfield_primaryColor,
          obscureText: widget.obscureText,
          keyboardType: widget.textInputType,
          textInputAction: widget.actionKeyboard,
          focusNode: widget.focusNode,
          style: TextStyle(
            color: textfield_textStyle,
            fontSize: 16.0,
            fontWeight: FontWeight.w400,
            fontStyle: FontStyle.italic,
            letterSpacing: 1.2,
          ),
          initialValue: widget.defaultText,
          onSaved: widget.onSaved,
          validator: widget.validator,
          onFieldSubmitted: widget.onFieldSubmitted,
          decoration: InputDecoration(
            prefixIcon: widget.prefixIcon,
            hintText: widget.hintText,
            labelText: widget.labelText,
            helperText: widget.helperText,
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(color: textfield_primaryColor),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: textfield_primaryColor),
            ),
            hintStyle: TextStyle(
              color: Colors.grey,
              fontSize: 18.0,
              fontWeight: FontWeight.w400,
              fontStyle: fontdesign,
              letterSpacing: 1.2,
            ),
            contentPadding: EdgeInsets.only(
                top: 12, bottom: bottomPaddingToError, left: 12.0, right: 12.0),
            isDense: true,
            errorStyle: TextStyle(
              color: textfield_errorStyle,
              fontSize: 12.0,
              fontWeight: FontWeight.w300,
              fontStyle: fontdesign,
              letterSpacing: 1.2,
            ),
            errorBorder: OutlineInputBorder(
              borderSide: BorderSide(color: textfield_primaryColor),
            ),
            focusedErrorBorder: OutlineInputBorder(
              borderSide: BorderSide(color: textfield_primaryColor),
            ),
          ),
          controller: widget.controller,
        ));
  }
}

String commonValidation(String value, String messageError) {
  var required = requiredValidator(value, messageError);
  if (required != null) {
    return required;
  }
  return null;
}

String requiredValidator(value, messageError) {
  if (value.isEmpty) {
    return messageError;
  }
  return null;
}

void changeFocus(
    BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
  currentFocus.unfocus();
  FocusScope.of(context).requestFocus(nextFocus);
}

/*----------------------------RAISED BUTTON-----------------------------------*/

ButtonTheme raisedButton(
    {VoidCallback onClick,
    String text,
    Color textColor,
    Color color,
    Color splashColor,
    double borderRadius,
    double minWidth,
    double height,
    Color borderSideColor,
    TextStyle style,
    Widget leadingIcon,
    Widget trailingIcon}) {
  return ButtonTheme(
    height: height,
    child: RaisedButton(
        splashColor: Colors.grey.withOpacity(0.5) ?? colorBlack,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(borderRadius ?? 25),
            side: BorderSide(color: borderSideColor ?? color)),
        textColor: Colors.white,
        color: button_primaryColor,
        child: Row(
          mainAxisSize: MainAxisSize.min,
          // This is must when you are using Row widget inside Raised Button
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            _buildLeadingIcon(leadingIcon),
            Text(
              text ?? '',
              style: TextStyle(
                color: Colors.white,
                fontSize: 14.0,
                fontWeight: FontWeight.w500,
                fontStyle: fontdesign,
                letterSpacing: 1.2,
              ),
            ),
            _buildtrailingIcon(trailingIcon),
          ],
        ),
        onPressed: () {
          return onClick();
        }),
  );
}

Widget _buildLeadingIcon(Widget leadingIcon) {
  if (leadingIcon != null) {
    return Row(
      children: <Widget>[leadingIcon, SizedBox(width: 10)],
    );
  }
  return Container();
}

Widget _buildtrailingIcon(Widget trailingIcon) {
  if (trailingIcon != null) {
    return Row(
      children: <Widget>[
        SizedBox(width: 10),
        trailingIcon,
      ],
    );
  }
  return Container();
}

/*-------------------------BACKGROUND WIDGET---------------------------------------*/

class BackgroundWidget extends StatelessWidget {
  final String imagepath;

  const BackgroundWidget({this.imagepath});

  @override
  Widget build(BuildContext context) {
    return Image.network(
      imagepath,
      fit: BoxFit.cover,
    );
  }
}
/*---------------------------FLAT BUTTON---------------------------------*/

class Flatbutton extends StatefulWidget {
  Flatbutton({
    this.color,
    this.colorBrightness,
    this.disabledColor,
    this.disabledTextcolor,
    this.focusColor,
    this.highlightColor,
    this.hoverColor,
    this.shape,
    this.splashColor,
    this.textcolor,
    this.child,
    this.onPressed,
  });

  Color textcolor;
  Color disabledTextcolor;
  Color color;
  Color disabledColor;
  Color focusColor;
  Color hoverColor;
  Color highlightColor;
  Color splashColor;
  Color colorBrightness;
  var shape;
  var child;
  var onPressed;
  @override
  _FlatbuttonState createState() => _FlatbuttonState();
}

class _FlatbuttonState extends State<Flatbutton> {
  @override
  Widget build(BuildContext context) {
    return FlatButton(
      color: widget.color,
      textColor: widget.textcolor,
      focusColor: widget.focusColor,
      hoverColor: widget.hoverColor,
      highlightColor: widget.highlightColor,
      splashColor: widget.splashColor,
      disabledColor: widget.disabledColor,
      disabledTextColor: widget.disabledTextcolor,
      shape: widget.shape,
      child: widget.child,
      onPressed: widget.onPressed,
    );
  }
}

/*-----------------------------------------------------------------*/

class Flutterlogo extends StatefulWidget {
  var size;

  Flutterlogo({this.size});

  @override
  _FlutterlogoState createState() => _FlutterlogoState();
}

class _FlutterlogoState extends State<Flutterlogo> {
  @override
  Widget build(BuildContext context) {
    return FlutterLogo(
      size: widget.size,
    );
  }
}
