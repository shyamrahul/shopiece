import 'package:auth/Page/Login/Login.dart';
import 'package:auth/Page/RegisterPage/second.dart';
import 'package:flutter/material.dart';
import 'package:sailor/sailor.dart';
import 'package:auth/Page/RegisterPage/register.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'Page/RegisterPage/third.dart';
import 'generated/l10n.dart';
import 'package:auth/Styling/styles.dart';
import 'package:device_preview/device_preview.dart';
void main() {
  Routes.createRoutes();
  runApp(
   MyApp(),
  );
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      localizationsDelegates: [
     const   AppLocalizationDelegate(),
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate
      ],
      supportedLocales: [Locale("en"), Locale("hi")],
      onGenerateTitle: (BuildContext context) =>
          S.of(context).tittle,
      theme: ThemeData(
        primarySwatch: primarySwatch,
      ),
      debugShowCheckedModeBanner: false,
      home: LoginPage(),
      onGenerateRoute: Routes.sailor.generator(),
      navigatorKey: Routes.sailor.navigatorKey,
    );
  }
}

class Routes {
  static final sailor = Sailor();

  static void createRoutes() {
    sailor.addRoutes([
      SailorRoute(
        name: 'register',
        builder: (context, args, params) {
          return RegisterPage();
        },
      ),
      SailorRoute(
        name: 'second',
        builder: (context, args, params) {
          return SecondPage();
        },
      ),
      SailorRoute(
        name: 'third',
        builder: (context, args, params) {
          return ThirdPage();
        },
      ),
      SailorRoute(
        name: 'loginpage',
        builder: (context, args, params) {
          return LoginPage();
        },
      ),
    ]);
  }
}
