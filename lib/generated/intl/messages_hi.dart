// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a hi locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'hi';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static _notInlinedMessages(_) => <String, Function> {
    "Uname" : MessageLookupByLibrary.simpleMessage("उपयोगकर्ता नाम"),
    "Valid1" : MessageLookupByLibrary.simpleMessage("मान्य ईमेल दर्ज करें"),
    "create" : MessageLookupByLibrary.simpleMessage("सृजन करना"),
    "fname" : MessageLookupByLibrary.simpleMessage("पूरा नाम"),
    "forgotpassword" : MessageLookupByLibrary.simpleMessage("पासवर्ड भूल गए"),
    "login" : MessageLookupByLibrary.simpleMessage("लॉग इन करें"),
    "name" : MessageLookupByLibrary.simpleMessage("ईमेल"),
    "next" : MessageLookupByLibrary.simpleMessage("आगे"),
    "password" : MessageLookupByLibrary.simpleMessage("कुंजिका"),
    "sg" : MessageLookupByLibrary.simpleMessage("साइन इन करें"),
    "sn" : MessageLookupByLibrary.simpleMessage("अगर आपके पास खाता नहीं है?"),
    "tittle" : MessageLookupByLibrary.simpleMessage("अगली पीड़ी"),
    "valid2" : MessageLookupByLibrary.simpleMessage("कृपया मान्य पासवर्ड"),
    "valid3" : MessageLookupByLibrary.simpleMessage("कृपया मान्य नाम दर्ज करें"),
    "valid4" : MessageLookupByLibrary.simpleMessage("कृपया मान्य उपयोगकर्ता नाम दर्ज करें")
  };
}
