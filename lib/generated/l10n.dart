// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars

class S {
  S();
  
  static S current;
  
  static const AppLocalizationDelegate delegate =
    AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false) ? locale.languageCode : locale.toString();
    final localeName = Intl.canonicalizedLocale(name); 
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      S.current = S();
      
      return S.current;
    });
  } 

  static S of(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `NEXT_GEN`
  String get tittle {
    return Intl.message(
      'NEXT_GEN',
      name: 'tittle',
      desc: '',
      args: [],
    );
  }

  /// `EMAIL`
  String get name {
    return Intl.message(
      'EMAIL',
      name: 'name',
      desc: '',
      args: [],
    );
  }

  /// `User Name`
  String get Uname {
    return Intl.message(
      'User Name',
      name: 'Uname',
      desc: '',
      args: [],
    );
  }

  /// `PASSWORD`
  String get password {
    return Intl.message(
      'PASSWORD',
      name: 'password',
      desc: '',
      args: [],
    );
  }

  /// `LOGIN`
  String get login {
    return Intl.message(
      'LOGIN',
      name: 'login',
      desc: '',
      args: [],
    );
  }

  /// `NEXT`
  String get next {
    return Intl.message(
      'NEXT',
      name: 'next',
      desc: '',
      args: [],
    );
  }

  /// `FULL NAME`
  String get fname {
    return Intl.message(
      'FULL NAME',
      name: 'fname',
      desc: '',
      args: [],
    );
  }

  /// `CREATE`
  String get create {
    return Intl.message(
      'CREATE',
      name: 'create',
      desc: '',
      args: [],
    );
  }

  /// `Forgot Password`
  String get forgotpassword {
    return Intl.message(
      'Forgot Password',
      name: 'forgotpassword',
      desc: '',
      args: [],
    );
  }

  /// `Enter Valid Email`
  String get Valid1 {
    return Intl.message(
      'Enter Valid Email',
      name: 'Valid1',
      desc: '',
      args: [],
    );
  }

  /// `Please valid password`
  String get valid2 {
    return Intl.message(
      'Please valid password',
      name: 'valid2',
      desc: '',
      args: [],
    );
  }

  /// `Please enter valid Name`
  String get valid3 {
    return Intl.message(
      'Please enter valid Name',
      name: 'valid3',
      desc: '',
      args: [],
    );
  }

  /// `Please enter valid User Name`
  String get valid4 {
    return Intl.message(
      'Please enter valid User Name',
      name: 'valid4',
      desc: '',
      args: [],
    );
  }

  /// `If you don't have account?`
  String get sn {
    return Intl.message(
      'If you don\'t have account?',
      name: 'sn',
      desc: '',
      args: [],
    );
  }

  /// `sign in`
  String get sg {
    return Intl.message(
      'sign in',
      name: 'sg',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'hi'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    if (locale != null) {
      for (var supportedLocale in supportedLocales) {
        if (supportedLocale.languageCode == locale.languageCode) {
          return true;
        }
      }
    }
    return false;
  }
}