import 'package:flutter/widgets.dart';

class SizeConfig {
  static MediaQueryData _mediaQueryData;
  static double height;
  static double width;
  static double screenWidth;
  static double screenHeight;
  static double blockSizeHorizontal;
  static double blockSizeVertical;
  static double _safeAreaHorizontal;
  static double _safeAreaVertical;
  static double safeBlockHorizontal;
  static double safeBlockVertical;

  void init(BuildContext context) {
    _mediaQueryData = MediaQuery.of(context);
    screenWidth = _mediaQueryData.size.width;
    screenHeight = _mediaQueryData.size.height;
    blockSizeHorizontal = screenWidth / 100;
    blockSizeVertical = screenHeight / 100;
    _safeAreaHorizontal =
        _mediaQueryData.padding.left + _mediaQueryData.padding.right;
    _safeAreaVertical =
        _mediaQueryData.padding.top + _mediaQueryData.padding.bottom;

    //double deviceWidth = mediaQuery.size.shortestSide;
    safeBlockHorizontal = (screenWidth - _safeAreaHorizontal) / 100;
    safeBlockVertical = (screenHeight - _safeAreaVertical) / 100;

    if (screenWidth > 950) {
      width = safeBlockVertical * 60; //desktop
      height = safeBlockHorizontal * 30;
    } else if (screenWidth > 800) {
      width = safeBlockVertical * 70; //tab
      height = safeBlockHorizontal * 30;
    } else if (screenWidth > 590) {
      width = safeBlockVertical * 45; //mob
      height = safeBlockHorizontal * 30; //up down
    } else if (screenWidth > 560) {
      width = safeBlockVertical * 80; //mob
      height = safeBlockHorizontal * 30; //up down
    } else if (screenWidth > 300) {
      width = safeBlockVertical * 40; //mob
      height = safeBlockHorizontal * 40; //up down
    }
  }
}
