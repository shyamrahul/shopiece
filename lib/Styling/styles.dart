import 'package:flutter/material.dart';

final FontStyle fontdesign = FontStyle.italic;
final Color textfield_textStyle = Color(0xff000000);
final Color textfield_errorStyle = Color(0xffC44137);
final Color textfield_primaryColor = Colors.blue;
final Color colorBlack = Color(0xff000000);
final Color button_primaryColor = Colors.blue;
final Color button_textcolor = Colors.white;
final Color button_borderSideColor = Colors.white;
final Color primarySwatch = Colors.blue;



